<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
<title>Add New Buku</title>
</head>
<body>
    <form action="BukuController.do" method="post">
        <fieldset>
            <div>
                <label for="idBuku">ID Buku</label> <input type="text"
                    name="idBuku" value="<c:out value="${buku.idBuku}" />"
                    readonly="readonly" placeholder="ID Buku" />
            </div>
            <div>
                <label for="judulBuku">Judul Buku</label> <input type="text"
                    name="judulBuku" value="<c:out value="${buku.judulBuku}" />"
                    placeholder="Judul Buku" />
            </div>
            <div>
                <label for="pengarangBuku">Pengarang Buku</label> <input type="text"
                    name="pengarangBuku" value="<c:out value="${buku.pengarangBuku}" />"
                    placeholder="Pengarang Buku" />
            </div>
            <div>
                <label for="isbn">ISBN</label> <input type="text" name="isbn"
                    value="<c:out value="${buku.isbn}" />" placeholder="ISBN" />
            </div>
            <div>
                <label for="jumlah">Jumlah Halaman</label> <input type="text" name="jumlah"
                    value="<c:out value="${buku.jumlah}" />" placeholder="Jumlah" />
            </div>
            <div>
                <input type="submit" value="Submit" />
            </div>
        </fieldset>
    </form>
</body>
</html>