<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show All Buku</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>ID Buku</th>
                <th>Judul Buku</th>
                <th>Pengarang Buku</th>
                <th>ISBN</th>
                <th>Jumlah</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${bukus}" var="buku">
                <tr>
                    <td><c:out value="${buku.idBuku}" /></td>
                    <td><c:out value="${buku.judulBuku}" /></td>
                    <td><c:out value="${buku.pengarangBuku}" /></td>
                    <td><c:out value="${buku.isbn}" /></td>
                    <td><c:out value="${buku.jumlah}" /></td>
                    <td><a
                        href="BukuController.do?action=edit&idBuku=<c:out value="${buku.idBuku}"/>">Update</a></td>
                    <td><a
                        href="BukuController.do?action=delete&idBuku=<c:out value="${buku.idBuku}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p>
        <a href="BukuController.do?action=insert">Add Buku</a>
    </p>
</body>
</html>