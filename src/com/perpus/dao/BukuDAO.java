package com.perpus.dao;

import java.util.List;

import com.perpus.model.Buku;

public interface BukuDAO 
{
	public void addBuku(Buku buku);
    public void deleteBuku(int idBuku);
    public void updateBuku(Buku buku);
    public List<Buku> getAllBukus();
    public Buku getBukuById(int idBuku);
}
