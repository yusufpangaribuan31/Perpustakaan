package com.perpus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import com.perpus.model.Buku;
import com.perpus.util.DBUtil;

public class BukuDAOImplementation implements BukuDAO 
{
	private Connection conn;
	
	public BukuDAOImplementation() 
	{
		conn = DBUtil.getConnection();
	}

	@Override
	public void addBuku(Buku buku) 
	{
		try
		{
			String query = "insert intu buku (judulBuku, pengarangBuku, isbn, jumlah) values (?,?,?,?)";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, buku.getJudulBuku());
			preparedStatement.setString(2, buku.getPengarangBuku());
			preparedStatement.setString(3, buku.getIsbn());
			preparedStatement.setInt(4, buku.getJumlah());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void deleteBuku(int idBuku) 
	{
		try
		{
			String query = "delete from buku where idBuku=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setInt(1, idBuku);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void updateBuku(Buku buku) 
	{
		try
		{
			String query = "update buku set judulBuku=?, pengarangBuku=?, isbn=?, jumlah=? where isBuku=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setString(1, buku.getJudulBuku());
			preparedStatement.setString(2, buku.getPengarangBuku());
			preparedStatement.setString(3, buku.getIsbn());
			preparedStatement.setInt(4, buku.getJumlah());
			preparedStatement.setInt(5, buku.getIdBuku());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public List<Buku> getAllBukus() 
	{
		List<Buku> bukus = new ArrayList<Buku>();
		try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery( "select * from buku" );
            while( resultSet.next() ) {
                Buku buku = new Buku();
                buku.setIdBuku( resultSet.getInt( "idBuku" ) );
                buku.setJudulBuku( resultSet.getString( "judulBuku" ) );
                buku.setPengarangBuku( resultSet.getString( "pengarangBuku" ) );
                buku.setIsbn( resultSet.getString( "isbn" ) );
                buku.setJumlah( resultSet.getInt( "jumlah" ) );
                bukus.add(buku);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bukus;
	}

	@Override
	public Buku getBukuById(int idBuku) 
	{
		Buku buku = new Buku();
        try 
        {
            String query = "select * from buku where idBuku=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idBuku);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) 
            {
                buku.setIdBuku(resultSet.getInt("idBuku"));
                buku.setJudulBuku(resultSet.getString("judulBuku"));
                buku.setPengarangBuku(resultSet.getString("pengarangBuku"));
                buku.setIsbn(resultSet.getString("isbn"));
                buku.setJumlah( resultSet.getInt("jumlah"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) 
        {
            e.printStackTrace();
        }
        return buku;
    }
		
}
