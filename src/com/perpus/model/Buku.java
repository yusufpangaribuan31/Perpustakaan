package com.perpus.model;

public class Buku 
{
	private int idBuku;
	private String judulBuku;
	private String pengarangBuku;
	private String isbn;
	private int jumlah;
	public int getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(int idBuku) {
		this.idBuku = idBuku;
	}
	public String getJudulBuku() {
		return judulBuku;
	}
	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}
	public String getPengarangBuku() {
		return pengarangBuku;
	}
	public void setPengarangBuku(String pengarangBuku) {
		this.pengarangBuku = pengarangBuku;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getJumlah() {
		return jumlah;
	}
	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	@Override
	public String toString() {
		return "Buku [idBuku=" + idBuku + ", judulBuku=" + judulBuku + ", pengarangBuku=" + pengarangBuku + ", isbn="
				+ isbn + ", jumlah=" + jumlah + "]";
	}
}
